/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */

package ch.jsel.privat.SpotifyEditor.viewmodel;

import ch.jsel.privat.SpotifyEditor.model.Model;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 *
 * @author Jekathmenan
 */
public class ViewModel implements PropertyChangeListener
{
    private final Model model;

    public ViewModel(Model model)
    {
        this.model = model;
    }
    
    public void minimizeView()
    {
        model.minimizeView("View" ,"view/view.fxml");
    }
    
    public void exitView()
    {
        model.exit();
    }
    
    public void changeView(String title, String view)
    {
        model.changeView(title, view);
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {
        
    }
}
