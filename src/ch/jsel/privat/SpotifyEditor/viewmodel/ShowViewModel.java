/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */
package ch.jsel.privat.SpotifyEditor.viewmodel;

import ch.jsel.privat.SpotifyEditor.entity.ShowData;
import ch.jsel.privat.SpotifyEditor.entity.SpotifyData;
import ch.jsel.privat.SpotifyEditor.entity.SpotifyUser;
import ch.jsel.privat.SpotifyEditor.model.Model;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Jekathmenan
 */
public class ShowViewModel implements PropertyChangeListener
{
    private final Model model;
    ArrayList<String> names = new ArrayList<>();
    private final ObservableList<ShowData> paymentData = FXCollections.observableArrayList();

    public ShowViewModel(Model model)
    {
        this.model = model;
    }
    
    public void minimizeView()
    {
        model.minimizeView("Show" ,"view/Show.fxml");
    }
    
    public void exitView()
    {
        model.exit();
    }
    
    public void backAction()
    {
        model.changeView("View", "view/view.fxml");
    }
    
    public void showNames()
    {
        model.showNames();
    }
    
    public void showPayments(String prename, String date)
    {
        model.getPayments(prename, date);
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent evt) throws NullPointerException
    {
        switch(evt.getPropertyName())
        {
            case "Prenames":
                names = (ArrayList<String>) evt.getNewValue();
                break;
            case "Data":
                paymentData.clear();
                try
                {
                    ArrayList<ShowData> data = new ArrayList<>();
                    data = (ArrayList<ShowData>) evt.getNewValue();
                    for (ShowData showData : data)
                    {
                        paymentData.add(showData);
                    }
                }
                catch(Exception ex)
                {
                    
                }
                
                break;
            default:
                break;
        }
    }

    public ArrayList<String> getNames()
    {
        //System.out.println(names.get(0));
        return names;
    }

    public ObservableList<ShowData> getPaymentData()
    {
        return paymentData;
    }
}
