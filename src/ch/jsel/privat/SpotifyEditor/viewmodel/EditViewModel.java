/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */

package ch.jsel.privat.SpotifyEditor.viewmodel;

import ch.jsel.privat.SpotifyEditor.model.Model;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

/**
 *
 * @author Jekathmenan
 */
public class EditViewModel implements PropertyChangeListener
{
    Model model;
    ArrayList<String> names = new ArrayList<>();

    public EditViewModel(Model model)
    {
        this.model = model;
    }
    
    public void minimizeView()
    {
        model.minimizeView("Edit" ,"view/Edit.fxml");
    }
    
    public void showNames()
    {
        model.showNames();
    }
    
    public void exitView()
    {
        model.exit();
    }
    
    public void backAction()
    {
        model.changeView("View", "view/view.fxml");
    }
    
    public void addPayment(String prename, boolean paid, int amount, String date)
    {
        model.addPayment(prename, paid, amount, date);
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {
        if(evt.getPropertyName().equals("Prenames"))
        {
            this.names = (ArrayList<String>) evt.getNewValue();
        }
    }

    public ArrayList<String> getNames()
    {
        return names;
    }
}
