/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.jsel.privat.SpotifyEditor;

import ch.jsel.privat.SpotifyEditor.Database.DatabaseConnector;
import ch.jsel.privat.SpotifyEditor.model.Model;
import ch.jsel.privat.SpotifyEditor.view.EditView;
import ch.jsel.privat.SpotifyEditor.view.ShowView;
import ch.jsel.privat.SpotifyEditor.view.View;
import ch.jsel.privat.SpotifyEditor.viewmodel.EditViewModel;
import ch.jsel.privat.SpotifyEditor.viewmodel.ShowViewModel;
import ch.jsel.privat.SpotifyEditor.viewmodel.ViewModel;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Jekathmenan
 */
public class MainApp extends Application
{

    private Model model;
    Stage secondaryStage;
    private FXMLLoader loader;
    private View view;
    private ShowView showView;
    private EditView editView;
    

    @Override
    public void start(Stage stage) throws Exception
    {
        //DatabaseConnector dao = new DatabaseConnector();
        //dao.readDatabase();

        loader = new FXMLLoader(getClass().getResource("view/view.fxml"));
        Parent root = loader.load();

        model = new Model(this);
        view = loader.getController();
        final ViewModel viewModel = new ViewModel(model);

        view.setViewModel(viewModel);
        model.addPropertyChangeListener(viewModel);
        view.bind();
        
        secondaryStage = stage;

        Scene scene = new Scene(root);

        stage.setResizable(true);
        stage.initStyle(StageStyle.UNDECORATED);

        stage.setScene(scene);
        stage.show();
    }

    public void minimizeView(String title, String url) throws IOException
    {
        loader = new FXMLLoader(getClass().getResource(url));
        Parent root = loader.load();
        Scene scene;

        // setting up the views
        switch (title)
        {
            case "View":
                view = loader.getController();
                final ViewModel viewModel = new ViewModel(model);
                view.setViewModel(viewModel);
                model.addPropertyChangeListener(viewModel);
                view.bind();
                break;

            case "Edit":
                editView = loader.getController();
                final EditViewModel editViewModel = new EditViewModel(model);
                editView.setViewModel(editViewModel);
                model.addPropertyChangeListener(editViewModel);
                editView.bind();
                break;

            case "Show":

                showView = loader.getController();
                final ShowViewModel showViewModel = new ShowViewModel(model);
                showView.setViewModel(showViewModel);
                model.addPropertyChangeListener(showViewModel);
                showView.bind();
                showView.showNames();
                break;
        }

        // All the binding need to be done here
        scene = new Scene(root);

        // minimize window
        secondaryStage.setIconified(true);

        secondaryStage.setScene(scene);
        secondaryStage.show();
    }

    public void exit()
    {
        System.exit(0);
    }

    public void changeView(String title, String url) throws IOException
    {
        loader = new FXMLLoader(getClass().getResource(url));
        Parent root = loader.load();
        Scene scene;

        // setting up the views
        switch (title)
        {
            case "View":
                view = loader.getController();
                final ViewModel viewModel = new ViewModel(model);
                view.setViewModel(viewModel);
                model.addPropertyChangeListener(viewModel);
                view.bind();
                break;

            case "Edit":
                editView = loader.getController();
                final EditViewModel editViewModel = new EditViewModel(model);
                editView.setViewModel(editViewModel);
                model.addPropertyChangeListener(editViewModel);
                editView.showNames();
                editView.bind();
                break;

            case "Show":
                showView = loader.getController();
                final ShowViewModel showViewModel = new ShowViewModel(model);
                showView.setViewModel(showViewModel);
                model.addPropertyChangeListener(showViewModel);
                
                showView.bind();
                showView.showNames();
                break;
        }

        // All the binding need to be done here
        scene = new Scene(root);

        // setting scene to not resizable and undecorated
        secondaryStage.setResizable(true);

        secondaryStage.setScene(scene);
        secondaryStage.show();
    }
    
    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception
    {
        launch(args);
    }
}