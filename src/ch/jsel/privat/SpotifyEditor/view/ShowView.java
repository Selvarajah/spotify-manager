/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.jsel.privat.SpotifyEditor.view;

import ch.jsel.privat.SpotifyEditor.entity.ShowData;
import ch.jsel.privat.SpotifyEditor.viewmodel.ShowViewModel;
import com.sun.javafx.binding.SelectBinding;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.beans.property.Property;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author Jekathmenan
 */
public class ShowView implements Initializable
{

    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private ShowViewModel viewModel;

    @FXML
    private ChoiceBox<String> chbPrename;
    @FXML
    private TableColumn<ShowData, String> name;
    @FXML
    private TableColumn<ShowData, String> prename;
    @FXML
    private TableColumn<ShowData, String> amount;
    @FXML
    private TableColumn<ShowData, String> payed;
    @FXML
    private TableColumn<ShowData, String> date;
    @FXML
    private TableView<ShowData> output;
    @FXML
    private DatePicker dtbDate;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {

    }

    public void bind()
    {
        output.setItems(viewModel.getPaymentData());
        addToTable();
    }

    private void addToTable()
    {
        name.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ShowData, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ShowData, String> n)
            {
                return new ReadOnlyObjectWrapper(n.getValue().getName().toString());
            }
        });

        prename.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ShowData, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ShowData, String> n)
            {
                return new ReadOnlyObjectWrapper(n.getValue().getPrename().toString());
            }
        });

        amount.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ShowData, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ShowData, String> n)
            {
                return new ReadOnlyObjectWrapper(n.getValue().getAmount().toString());
            }
        });

        payed.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ShowData, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ShowData, String> n)
            {
                return new ReadOnlyObjectWrapper<String>(n.getValue().getPaid())
                {
                };
            }
        });

        date.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ShowData, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ShowData, String> n)
            {
                return new ReadOnlyObjectWrapper(n.getValue().getDate().toString());
            }
        });
    }

    public void showNames()
    {
        viewModel.showNames();
        for (String name : viewModel.getNames())
        {
            String cap = name.substring(0, 1).toUpperCase() + name.substring(1);
            System.out.println(name);
            chbPrename.getItems().add(cap);
        }
    }

    public void setViewModel(ShowViewModel viewModel)
    {
        this.viewModel = viewModel;
    }

    @FXML
    private void minimizeView(MouseEvent event)
    {
        viewModel.minimizeView();
    }

    @FXML
    private void exitView(MouseEvent event)
    {
        viewModel.exitView();
    }

    @FXML
    private void backAction(MouseEvent event)
    {
        viewModel.backAction();
    }

    @FXML
    private void cancelAction(ActionEvent event)
    {
    }

    @FXML
    private void showPayments(ActionEvent event)
    {
        String dt = null;

        if (dtbDate.getValue() != null && chbPrename.getValue() != null)
        {
            dt = dtbDate.getValue().format(dateFormat);
        }
        viewModel.showPayments(chbPrename.getValue(), dt);
    }
}
