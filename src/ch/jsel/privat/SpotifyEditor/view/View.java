/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.jsel.privat.SpotifyEditor.view;

import ch.jsel.privat.SpotifyEditor.viewmodel.ViewModel;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Jekathmenan
 */
public class View implements Initializable
{
    private ViewModel viewModel;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // TODO
    }    
    
    public void bind()
    {
        
    }

    @FXML
    private void minimizeAction(MouseEvent event)
    {
        viewModel.minimizeView();
    }

    @FXML
    private void exitAction(MouseEvent event)
    {
        viewModel.exitView();
    }

    @FXML
    private void viewAction(MouseEvent event)
    {
        viewModel.changeView("Show", "view/Show.fxml");
    }

    @FXML
    private void editAction(MouseEvent event)
    {
        viewModel.changeView("Edit", "view/Edit.fxml");        
    }
    
    public void setViewModel(ViewModel viewModel)
    {
        this.viewModel = viewModel;
    }
}
