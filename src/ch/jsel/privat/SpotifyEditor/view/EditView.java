/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.jsel.privat.SpotifyEditor.view;

import ch.jsel.privat.SpotifyEditor.viewmodel.EditViewModel;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Jekathmenan
 */
public class EditView implements Initializable
{

    private EditViewModel viewModel;
    @FXML
    private CheckBox ckbPayed;
    @FXML
    private ChoiceBox<String> chbPrename;
    @FXML
    private TextField txtAmount;
    @FXML
    private DatePicker dtbDate;
    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // TODO
    } 
    
    public void bind()
    {
        
    }
    
    public void showNames()
    {
        viewModel.showNames();
        for (String name : viewModel.getNames())
        {
            String cap = name.substring(0, 1).toUpperCase() + name.substring(1);
            chbPrename.getItems().add(cap);
        }
    }

    public void setViewModel(EditViewModel viewModel)
    {
        this.viewModel = viewModel;
    }

    @FXML
    private void minimizeView(MouseEvent event)
    {
        viewModel.minimizeView();
    }

    @FXML
    private void exitView(MouseEvent event)
    {
        viewModel.exitView();
    }

    @FXML
    private void backAction(MouseEvent event)
    {
        viewModel.backAction();
    }

    @FXML
    private void payedAction(ActionEvent event)
    {
        ckbPayed.textProperty().set("Yes"); 
    }

    @FXML
    private void insertAction(ActionEvent event)
    {
        String date = dtbDate.getValue().format(dateFormat);
        viewModel.addPayment(chbPrename.getValue(), ckbPayed.isSelected(), Integer.valueOf(txtAmount.getText()), date);
        chbPrename.getItems().clear();
        showNames();
        txtAmount.clear();
        ckbPayed.setSelected(false);
        dtbDate.getEditor().clear();
        dtbDate.setValue(null);
    }

    @FXML
    private void cancelAction(ActionEvent event)
    {
        chbPrename.getItems().clear();
        showNames();
        txtAmount.clear();
        ckbPayed.setSelected(false);
        dtbDate.getEditor().clear();
        dtbDate.setValue(null);
    }
}