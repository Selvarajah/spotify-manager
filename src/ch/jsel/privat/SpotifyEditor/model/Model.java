/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */

package ch.jsel.privat.SpotifyEditor.model;

import ch.jsel.privat.SpotifyEditor.Database.Database;
import ch.jsel.privat.SpotifyEditor.MainApp;
import ch.jsel.privat.SpotifyEditor.entity.ShowData;
import ch.jsel.privat.SpotifyEditor.entity.SpotifyUser;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jekathmenan
 */
public class Model 
{
    private final MainApp mainApp;
    protected PropertyChangeSupport changes = new PropertyChangeSupport(this);
    private List<SpotifyUser> user = new ArrayList<>();
    private final Database db = new Database();
    
    public Model(MainApp mainApp)
    {
        this.mainApp = mainApp;
        addUser();
    }
    
    /**
     * 
     * A method to minimize the view
     * 
     * Calls mainApp's minimizeView
     * 
     * @param title
     * @param url 
     */
    public void minimizeView(String title, String url)
    {
        try
        {
            mainApp.minimizeView(title ,url);
        } catch (IOException ex)
        {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * A method to exit View
     * 
     * Calls mainApp's exit
     * 
     */
    public void exit()
    {
        mainApp.exit();
    }
    
    /**
     * 
     * A method to change the view
     * 
     * Calls mainapp's changeView
     * 
     * @param title
     * @param url 
     */
    public void changeView(String title ,String url)
    {
        try
        {
            mainApp.changeView(title, url);
        } 
        catch (IOException ex)
        {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * A method to show names
     * 
     * Note: calls db's getUser to fill users if null
     * 
     */
    public void showNames()
    {
        if(user.isEmpty())
        {
            user = db.getUser();
        }
        ArrayList<String> names = new ArrayList<>();
        for (SpotifyUser spotifyUser : user)
        {
            names.add(spotifyUser.getPrename());
        }
        changes.firePropertyChange("Prenames", null, names);
    }
    
    /**
     * 
     * A method to get names from database
     * 
     */
    public void addUser()
    {
        try
        {
            user = db.getUser();
        }
        catch(Exception ex)
        {
                    
        }
    }
    
    public void addPayment(String prename, boolean paid, int amount, String date)
    {
        try
        {
            int userid = 0;
            for (SpotifyUser spotifyUser : user)
            {
                if(spotifyUser.getPrename().toLowerCase().equals(prename.toLowerCase()))
                {
                    userid = spotifyUser.getUsersiD();
                }
            }
            if(paid)
            {
                db.addPayment(userid, 1, amount, date);
            }
            else
            {
                db.addPayment(userid, 0, amount, date);
            }
        }
        catch(Exception ex)
        {
            
        }
    }
    
    public void getPayments(String prename, String date)
    {
        boolean hasUser = false;
        ArrayList<ShowData> data = new ArrayList<>();
        for (SpotifyUser spotifyUser : user)
        {
            if(spotifyUser.getPrename().toLowerCase().equals(prename.toLowerCase()))
            {
                data = db.getPayments(spotifyUser.getUsersiD(), date);
                hasUser = true;
            }
        }
        
        if(!hasUser)
        {
            data = db.getPayments(-1, date);
        }
        changes.firePropertyChange("Data", null, data);
    }
    
    public void addPropertyChangeListener(PropertyChangeListener l)
    {
        changes.addPropertyChangeListener(l);
    }           
}