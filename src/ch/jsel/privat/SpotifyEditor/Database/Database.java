/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */

package ch.jsel.privat.SpotifyEditor.Database;

import ch.jsel.privat.SpotifyEditor.entity.*;
import java.util.ArrayList;

/**
 *
 * @author Jekathmenan
 */
public class Database 
{
    
    DatabaseConnector con = DatabaseConnector.getInstance();
    MySQLQuery query = new MySQLQuery(con);

    public ArrayList<SpotifyUser> getUser()
    {
        return query.getUser();
    }
    
    public void addPayment(int userid, int paid, int amount, String date)
    {
        query.addPayments(userid, paid, amount, date);
    }
    
    public ArrayList<ShowData> getPayments(int userid, String date)
    {
        if(userid == -1)
        {
            if(date == null)
            {
                return null;
            }
            return query.getPayments(date);
        }
        else
        {
            if(date == null)
            {
                return query.getPayments(userid);
            }
            return query.getPayments(userid, date);
        }
    }
}
