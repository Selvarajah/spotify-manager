/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */
package ch.jsel.privat.SpotifyEditor.Database;

import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jekathmenan
 */
public class DatabaseConnector
{
    private static final DatabaseConnector instance = null;
    private final String USERNAME = "sqluser";
    private final String PASSW0RD = "aklf84i8eranfd.";
    private final String DB_CONNECTION_STRING = "jdbc:mysql://127.0.0.1:3306/spotifylist?zeroDateTimeBehavior=convertToNull";    
    private final String DRIVER = "com.mysql.jdbc.Driver";
    private java.sql.Connection connection = null;
    
    private DatabaseConnector()
    {
        
    }
    
    public java.sql.Connection createConnection()
    {
        if(connection == null)
        {
            try 
            {
                Class.forName(DRIVER);
                connection = (java.sql.Connection) DriverManager.getConnection(DB_CONNECTION_STRING, USERNAME, PASSW0RD);
                System.out.println("Connected succesfully!");
            } 
            catch (ClassNotFoundException ex) 
            {
                System.out.println("Connection failed: Driver class not found!");
            }
            catch (SQLException ex)
            {
                System.out.println("Connection failed: SQLException occured!");
            }
        }
        return connection;
    }
    
    public void closeConnection() throws SQLException
    {
        connection.close();
        connection = null;
    }

    public static DatabaseConnector getInstance()
    {
        if(instance == null)
        {
            return new DatabaseConnector();
        }
        return instance;
    }
}
