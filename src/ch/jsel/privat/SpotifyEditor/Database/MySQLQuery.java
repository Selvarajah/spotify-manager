/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */
package ch.jsel.privat.SpotifyEditor.Database;

import ch.jsel.privat.SpotifyEditor.entity.ShowData;
import ch.jsel.privat.SpotifyEditor.entity.SpotifyData;
import ch.jsel.privat.SpotifyEditor.entity.SpotifyUser;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Jekathmenan
 */
public class MySQLQuery
{

    private final DatabaseConnector connector;
    private String query = "";
    private Connection conn;
    private PreparedStatement statement;

    public MySQLQuery(DatabaseConnector connector)
    {
        this.connector = connector;
    }

    public void addPayments(int usersiD, int paid, int amount, String date)
    {
        try
        {
            // creating query to execute and creating connection to database
            query = "INSERT INTO zahlung (usersiD, bezahlt, betrag, datum) VALUES(" + usersiD + ", " + paid + ", " + amount + ", " + "'" + date + "')";
            conn = connector.createConnection();

            // preparing and executing statement
            statement = conn.prepareStatement(query);
            statement.executeUpdate();

            // closing all
            statement.close();
            conn.close();
            connector.closeConnection();
        } 
        catch (Exception ex)
        {
            System.out.println("Could not add payment to database!;");
        }
    }

    public void deletePayment(int usersiD, int paid, int amount, String date)
    {
        try
        {
            // creating query and creating connection to database
            query = "DELETE FROM zahlung WHERE (usersiD =" + usersiD + " AND bezahlt =" + paid + " AND betrag =" + amount + " AND datum = '" + date + "')";
            conn = connector.createConnection();

            // preparing and executing statement
            statement = conn.prepareStatement(query);
            statement.executeUpdate();

            // closing all
            statement.close();
            conn.close();
            connector.closeConnection();
        }
        catch (Exception ex)
        {
            System.out.println("Could not delete payment from database!");
        }
    }
    
    public ArrayList<ShowData> getPayments(int usersiD)
    {
        try
        {
            query = "CALL getPaymentsWithoutDate(" + usersiD + ")";
            conn = connector.createConnection();
            statement = conn.prepareStatement(query);
            
            ResultSet result = statement.executeQuery();
            ArrayList<ShowData> results = new ArrayList<>();
            
            while(result.next())
            {
                String prename = result.getString(1);
                String name = result.getString(2);
                int amount = result.getInt(3);
                boolean paid = result.getBoolean(4);
                String dt = result.getString(5);
                
                results.add(new ShowData(name,prename,amount, paid, dt));
            }
            return results;
        }
        catch(Exception ex)
        {
            
        }
        return null;
    }
    
    public ArrayList<ShowData> getPayments(String date)
    {
        try
        {
            query = "CALL getPaymentsWithoutUsersID(" + date + ")";
            conn = connector.createConnection();
            statement = conn.prepareStatement(query);
            
            ResultSet result = statement.executeQuery();
            ArrayList<ShowData> results = new ArrayList<>();
            
            while(result.next())
            {
                String prename = result.getString(1);
                String name = result.getString(2);
                int amount = result.getInt(3);
                boolean paid = result.getBoolean(4);
                String dt = result.getString(5);
                
                results.add(new ShowData(name,prename,amount, paid, dt));
            }
            return results;
        }
        catch(Exception ex)
        {
            
        }
        return null;
    }
    
    public ArrayList<ShowData> getPayments(int usersiD, String date)
    {
        try
        {
            query = "CALL getPayments(" + usersiD + ", " + date + ")";
            conn = connector.createConnection();
            statement = conn.prepareStatement(query);
            
            ResultSet result = statement.executeQuery();
            ArrayList<ShowData> results = new ArrayList<>();
            
            while(result.next())
            {
                String prename = result.getString(1);
                String name = result.getString(2);
                int amount = result.getInt(3);
                boolean paid = result.getBoolean(4);
                String dt = result.getString(5);
                
                results.add(new ShowData(name,prename,amount, paid, dt));
            }
            return results;
        }
        catch(Exception ex)
        {
            
        }
        return null;
    }
    
    public ArrayList<SpotifyUser> getUser()
    {
        try
        {
            query = "CALL getNames()";
            conn = connector.createConnection();
            statement = conn.prepareStatement(query);
            
            ResultSet result = statement.executeQuery();
            ArrayList<SpotifyUser> results = new ArrayList<>();
            //System.out.println(result.getString(3));
            
            while(result.next())
            {
                int id = result.getInt(1);
                String prename = result.getString(3);
                String name = result.getString(2);
                System.out.println(prename);
                
                results.add(new SpotifyUser(id, prename, name));
            }
            return results;
        }
        catch(Exception ex)
        {
            System.out.println("Could not get user!");
        }
        return null;
    }
}