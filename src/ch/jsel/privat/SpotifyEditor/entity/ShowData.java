/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */

package ch.jsel.privat.SpotifyEditor.entity;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Jekathmenan
 */
public class ShowData 
{
    private final String name;
    private final String prename;
    private final String amount;
    private final String paid;
    private final String date;

    public ShowData(String name, String prename, int amount, boolean paid, String date)
    {
        this.name = name.substring(0, 1).toUpperCase() + name.substring(1);
        this.prename = prename.substring(0, 1).toUpperCase() + prename.substring(1);
        this.amount = Integer.toString(amount);
        this.date = date;
        
        if(paid)
        {
            this.paid =("Yes");
        }
        else
        {
            this.paid = ("No");
        }
    }

//    public StringProperty getName()
//    {
//        return name;
//    }
//
//    public StringProperty getPrename()
//    {
//        return prename;
//    }
//
//    public StringProperty getAmount()
//    {
//        return amount;
//    }
//
//    public StringProperty getPaid()
//    {
//        return paid;
//    }
//
//    public StringProperty getDate()
//    {
//        return date;
//    }

    public String getName()
    {
        return name;
    }

    public String getPrename()
    {
        return prename;
    }

    public String getAmount()
    {
        return amount;
    }

    public String getPaid()
    {
        return paid;
    }

    public String getDate()
    {
        return date;
    }
    
    
}
