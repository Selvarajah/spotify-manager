/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */

package ch.jsel.privat.SpotifyEditor.entity;

import java.sql.Date;

/**
 *
 * @author Jekathmenan
 */
public class SpotifyData 
{
    private boolean paid;
    private final int amount;
    private final String date;

    public SpotifyData(int amount, String date)
    {
        this.amount = amount;
        this.date = date;
    }

    public SpotifyData(boolean bezahlt, int betrag, String date)
    {
        this.paid = bezahlt;
        this.amount = betrag;
        this.date = date;
    }

    public void setBezahlt(boolean bezahlt)
    {
        this.paid = bezahlt;
    }

    public boolean isPaid()
    {
        return paid;
    }

    public int getAmount()
    {
        return amount;
    }

    public String getDate()
    {
        return date;
    }
}
