/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */

package ch.jsel.privat.SpotifyEditor.entity;

import java.util.*;

/**
 *
 * @author Jekathmenan
 */
public class SpotifyUser 
{
    private final String prename;
    private final String name;
    private final int usersiD;

    public SpotifyUser(int id, String prename, String name)
    {
        this.usersiD = id;
        this.prename = prename;
        this.name = name;
    }

    public String getPrename()
    {
        return prename;
    }

    public String getName()
    {
        return name;
    }

    public int getUsersiD()
    {
        return usersiD;
    }
}
